#!/usr/bin/env python

import roslib; roslib.load_manifest('visualization_marker_tutorials')
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import rospy
import math

import os
import csv
import pdb
import numpy as np
from my_pkg.msg import two_ints
from std_msgs.msg import Int16

destination = [0,0]

def callback(msg):

    global destination
    destination = [msg.a, msg.b]
    return destination


topic = 'visualization_marker_array'
sub = rospy.Subscriber('/two_ints', two_ints, callback, queue_size=1)
publisher = rospy.Publisher(topic, MarkerArray, queue_size="1")



rospy.init_node('view_node')

while not rospy.is_shutdown():

    markerArray = MarkerArray()
    x = float(destination[0])
    y = float(destination[1])
    marker = Marker()
    marker.header.frame_id = "/map"
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 0.11
    marker.scale.y = 0.11
    marker.scale.z = 0.11
    marker.color.a = 1.0
    marker.color.r = 1.0
    marker.color.g = 0.7
    marker.color.b = 0.8
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = x
    marker.pose.position.y = y
    marker.pose.position.z = 0
    marker.id = 0
    markerArray.markers.append(marker)

    publisher.publish(markerArray)

    rospy.sleep(0.3)

